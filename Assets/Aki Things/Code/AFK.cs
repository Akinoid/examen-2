using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AFK : MonoBehaviour
{
    int tiempo;
    public GameObject Afk;
    public GameObject Interfaz;

    void Update()
    {
        Vuelve();
        Desaparecido();

        if (tiempo > 2000)
        {
            Interfaz.SetActive(false);
            Afk.SetActive(true);
        }
    }
  
    void Vuelve()
    {
        if (Input.anyKeyDown)
        {
            Afk.SetActive(false);
            Interfaz.SetActive(true);
        }
    }

    void Desaparecido()
    {
        if (!Input.anyKeyDown)
        {
            tiempo++;
        }
        if (Input.anyKeyDown)
        {
            tiempo = 0;
        }
    }
}

