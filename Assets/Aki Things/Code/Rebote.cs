using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rebote : MonoBehaviour
{
    public Rigidbody2D RB;
    Vector2 guardar;
    public float movespeed;
    public Vector2 P;
    public bool isMoving;
    void Start()
    {
        RB = GetComponent<Rigidbody2D>();
        guardar = P;
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving == false)
        {
            RB.velocity = Vector2.zero;
        }
        else
        {
            RB.velocity = P * movespeed;

        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Coin"))
        {
            movespeed = movespeed * 2;
        }
        if (collision.gameObject.CompareTag("OffCamara"))
        {
            P = -P;
        }
        if (collision.gameObject.CompareTag("Marbles"))
        {
            isMoving = !isMoving;
        }
    }

}