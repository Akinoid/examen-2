using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{

    public ChangeSceneButton[] changeSceneButtons;

    private void Awake()
    {

        for (int i = 0; i < changeSceneButtons.Length; i++)
        {
            string sceneName = changeSceneButtons[i].sceneName;
            changeSceneButtons[i].button.onClick.AddListener(() =>
            {
                SceneManager.LoadScene(sceneName);

            });
        }

    }


}