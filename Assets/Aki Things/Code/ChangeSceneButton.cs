using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[Serializable]
public class ChangeSceneButton
{
    public Button button;
    public string sceneName;
}
