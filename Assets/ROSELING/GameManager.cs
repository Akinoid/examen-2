using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public int puntosObjetivo = 10;
    public TextMeshProUGUI puntosText;

    private int puntos;

    private void Start()
    {
        puntos = 0;
        UpdatePuntosText();
    }

    public void SumarPuntos(int cantidad)
    {
        puntos += cantidad;
        UpdatePuntosText();

        if (puntos >= puntosObjetivo)
        {
            VictoryScreen();
        }
    }

    public void RestarPuntos(int cantidad)
    {
        puntos -= cantidad;
        UpdatePuntosText();
    }

    private void UpdatePuntosText()
    {
        puntosText.text = "Puntos: " + puntos.ToString();
    }

    private void VictoryScreen()
    {
        // Carga la escena de victoria
        SceneManager.LoadScene("VICTORIA");
    }
}