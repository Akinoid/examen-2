using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Monedas : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (gameManager != null)
            {
                gameManager.RestarPuntos(1);
            }
            else
            {
                Debug.LogError("No se encontr� el componente GameManager en la escena.");
            }

           
            Destroy(gameObject, 0.5f);
        }
    }
}
