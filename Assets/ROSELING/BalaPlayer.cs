using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaPlayer : MonoBehaviour
{
    void OnEnable()
    {
         
        Destroy(gameObject, 3f);
    }

 
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Bullet")
        {
             
            Destroy(gameObject);
        }


        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject);
        }

        
    }
}
