using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisparoPlayer : MonoBehaviour
{
     
    public GameObject bulletObj;
    public float bulletSpeed;
    public Transform firePoint;
    Vector2 mousePosition;
    Rigidbody2D rb;
    Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        cam = FindObjectOfType<Camera>();
    }

    void Update()
    {
        mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            Disparar();
        }
    }
    void FixedUpdate()
    {
        //Direccion de vista del player = posicion del mouse - la posicion de player
        Vector2 lookDir = mousePosition - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
    }
    void Disparar()
    {
        GameObject bullet = Instantiate(bulletObj, firePoint.position, firePoint.rotation);
        Rigidbody2D rigidb = bullet.GetComponent<Rigidbody2D>();
        rigidb.AddForce(firePoint.up * bulletSpeed * 10f, ForceMode2D.Force);
    }
}
