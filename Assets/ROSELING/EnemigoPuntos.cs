using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemigoPuntos : MonoBehaviour
{
    public int puntos = 1;

    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.instance;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bala"))
        {
            if (collision.CompareTag("Bala"))
            {
                GameManager gameManager = FindObjectOfType<GameManager>();
                if (gameManager != null)
                {
                    gameManager.SumarPuntos(puntos);
                }
                else
                {
                    Debug.LogError("No se encontr� el componente GameManager en la escena.");
                }

                Destroy(gameObject);
                Destroy(collision.gameObject);
            }
        }// sufri para que logre encontrar el objeto :C 
    }
}