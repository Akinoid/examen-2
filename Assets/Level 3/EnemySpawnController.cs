using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController : MonoBehaviour
{
    public float delayBeforeActivation = 2f; // Tiempo de retraso antes de activar el componente de disparo

    private EnemyLevel3 enemyShooter; // Referencia al componente EnemyShooter

    private void Awake()
    {
        enemyShooter = GetComponent<EnemyLevel3>();
    }

    private void Start()
    {
        Invoke("ActivateShooter", delayBeforeActivation);
    }

    private void ActivateShooter()
    {
        enemyShooter.enabled = true;
    }
}
