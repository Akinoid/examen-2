using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 100; // Salud m�xima del jugador
    public int currentHealth; // Salud actual del jugador

    void Start()
    {
        currentHealth = maxHealth; // Inicializa la salud actual con la salud m�xima al inicio del juego
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage; // Resta el da�o recibido a la salud actual

        if (currentHealth <= 0)
        {
            Destroy(gameObject); // Si la salud actual es igual o menor a 0, el jugador muere
        }
    }

    
}
