using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f; // Velocidad de movimiento del jugador

    private Rigidbody2D playerRigidbody; // Referencia al Rigidbody2D del jugador

    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>(); // Obtener el componente Rigidbody2D del jugador
    }

    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal"); // Obtener el valor de entrada horizontal (eje X)
        float moveVertical = Input.GetAxis("Vertical"); // Obtener el valor de entrada vertical (eje Y)

        Vector2 movement = new Vector2(moveHorizontal, moveVertical); // Crear un vector de movimiento

        playerRigidbody.velocity = movement * moveSpeed; // Aplicar velocidad al Rigidbody2D del jugador
    }
}
