using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBullet : MonoBehaviour
{
    private ContadoLevel3 contadorScript; // Referencia al script de contador

    private void Start()
    {
        contadorScript = FindObjectOfType<ContadoLevel3>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {

            Destroy(other.gameObject);
        }
        else
        {

            contadorScript.DecrementarContador();
        }

        // Destruir la bala
        Destroy(gameObject);

        
    }
}
