using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyLevel3 : MonoBehaviour
{
    public GameObject bulletPrefab; // Prefab de la bala
    public float bulletSpeed = 5f; // Velocidad de la bala
    public float bulletLifetime = 10f; // Tiempo que tarda la bala en destruirse
    public Transform bulletSpawn; // Punto de spawn de la bala
    public Transform player; // Referencia al transform del jugador
    

    public int balasres = 10;
    private int contador;
    public Text conText;

   
    private void Start()
    {
        InvokeRepeating("Shoot", 1f, 1f); // Llama a la funci�n Shoot cada 1 segundo
    }

    void Shoot()
    {
        // Instancia la bala en el punto de spawn
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);

        // Calcula la direcci�n hacia el jugador
        Vector3 direction = (player.position - bulletSpawn.position).normalized;

        // Calcula el �ngulo de rotaci�n hacia la direcci�n
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        // Aplica la rotaci�n a la bala
        bullet.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        // Establece la velocidad de la bala
        bullet.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

        // Destruye la bala despu�s de un tiempo determinado
        Destroy(bullet, bulletLifetime);

        
    }

    public void baladestruida()
    {
        balasres--;


        if ( gameObject.activeSelf)
        {
            balasres++;
            
        }

        if (balasres >= 10)
        {
            SceneManager.LoadScene("Roseling - Alumno 6");

        }


    }
}

