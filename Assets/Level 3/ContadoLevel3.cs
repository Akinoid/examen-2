using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class ContadoLevel3 : MonoBehaviour
{
    private int contador =  100; // Cantidad inicial de balas que debe soportar el jugador

    public int Contador
    {
        get { return contador; }
    }

    public void DecrementarContador()
    {
        contador--;

        if (contador <= 0)
        {
            SceneManager.LoadScene("Roseling - Alumno 6");
        }
    }
}

    

