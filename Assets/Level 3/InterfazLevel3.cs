using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfazLevel3 : MonoBehaviour
{
    public TextMesh bulletCountText; // Referencia al componente Text donde se mostrar� el contador
    private int bulletCount = 0; // Contador de balas disparadas

    void Start()
    {
        bulletCountText.text = "Bullets: 250"; // Inicializa el texto del contador con el valor inicial de balas
    }

    public void IncreaseBulletCount()
    {
        bulletCount++; // Incrementa el contador de balas
        UpdateBulletCountText(); // Actualiza el texto del contador
    }

    public void DecreaseBulletCount()
    {
        bulletCount--; // Decrementa el contador de balas
        UpdateBulletCountText(); // Actualiza el texto del contador
    }

    void UpdateBulletCountText()
    {
        bulletCountText.text = "Balas Restantes: " + bulletCount.ToString(); // Actualiza el texto del contador con el valor actualizado de balas
    }
}
