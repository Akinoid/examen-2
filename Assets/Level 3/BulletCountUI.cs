using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletCountUI : MonoBehaviour
{
    public Text bulletCountText; // Referencia al texto de la interfaz
    public int initialBulletCount = 300; // Cantidad inicial de balas
    private int bulletCount; // Contador de balas
    private float remainingTime; // Tiempo restante del contador

    private int bulletCountGlobal; // Contador de balas global

    void Start()
    {
        bulletCount = initialBulletCount;
        remainingTime = initialBulletCount;
        bulletCountGlobal = initialBulletCount;
        UpdateBulletCountText();
    }

    void Update()
    {
        remainingTime -= Time.deltaTime;
        if (remainingTime <= 0)
        {
            remainingTime = 0;
        }

        UpdateBulletCountText();
    }

    public void UpdateBulletCountText()
    {
        bulletCountText.text = "Balas: " + bulletCountGlobal.ToString() + " | Time: " + Mathf.RoundToInt(remainingTime).ToString();
    }

    public void DecreaseBulletCount()
    {
        bulletCount--;
        bulletCountGlobal--;
        UpdateBulletCountText();
    }
}
