using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;


public class ScoreText : MonoBehaviour
{
    public TextMeshProUGUI contadorText;
    public ContadoLevel3 contadorScript;

    private void Start()
    {
        contadorScript = FindObjectOfType<ContadoLevel3>();
    }

    private void Update()
    {

        int contador = contadorScript.Contador;

        contadorText.text = "Balas : " + contador.ToString();

        if (contador <= 0)
        {
            SceneManager.LoadScene("Roseling - Alumno 6");
        }
    }

}
