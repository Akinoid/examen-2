using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnLevel3 : MonoBehaviour
{
    public GameObject enemyPrefab; // Prefab del enemigo
    public Transform spawnPoint; // Punto de spawn del enemigo
    public int totalEnemies = 2; // Cantidad total de enemigos a spawnear
    public float spawnInterval = 2.5f; // Intervalo de tiempo entre spawns
    public float spawnSpacing = 2f;

    private int spawnedEnemies = 0; // Cantidad de enemigos ya spawneados

    private void Start()
    {
        // Comienza a spawnear enemigos
        StartCoroutine(SpawnEnemies());
    }

    private IEnumerator SpawnEnemies()
    {
        while (spawnedEnemies < totalEnemies)
        {
            // Espera el intervalo de tiempo antes de spawnear un nuevo enemigo
            yield return new WaitForSeconds(spawnInterval);

            // Instancia un nuevo enemigo en el punto de spawn
            Instantiate(enemyPrefab, spawnPoint.position, Quaternion.identity);

            // Incrementa el contador de enemigos spawneados
            spawnedEnemies++;
        }
    }

}
