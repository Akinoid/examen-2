using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparocontra : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float shootInterval = 1f;
    public float bulletSpeed = 5f;

    private float timer = 0f;

   
    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= shootInterval)
        {
            Shoot();
            timer = 0f;
        }
    }

    public void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        bullet.GetComponent<Rigidbody2D>().velocity = Vector2.right * bulletSpeed;
        bullet.GetComponent<Collider2D>().isTrigger = true;
    }
}
