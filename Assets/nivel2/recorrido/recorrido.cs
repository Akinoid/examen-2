using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using TMPro;

public class recorrido : MonoBehaviour
{
    public Transform player;
    public TMP_Text distanceText;

    private float initialY;

    private void Start()
    {
        initialY = player.position.y;
    }

    private void Update()
    {
        float currentDistance = Mathf.Max(0f, player.position.y - initialY);
        distanceText.text = "Distancia Recorrida: " + currentDistance.ToString("F0");
    }
}
