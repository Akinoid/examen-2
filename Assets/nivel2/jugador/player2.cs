using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player2 : MonoBehaviour
{
    public float speed = 5f;

    private void Update()
    {
        
        float moveHorizontal = Input.GetAxis("Horizontal");

        
        float moveVertical = Mathf.Max(0f, Input.GetAxis("Vertical"));

        
        Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0f) * speed * Time.deltaTime;

        
        transform.Translate(movement);
    }
}
