using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject objectEnemy;
    public float time;
    public float maxTime;

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time >= maxTime)
        {
            GameObject enemigo = Instantiate(objectEnemy);
            enemigo.transform.position = transform.position;
            time = 0;
        }
    }
}
