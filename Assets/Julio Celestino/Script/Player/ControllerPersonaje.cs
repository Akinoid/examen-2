using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerPersonaje : MonoBehaviour
{
    public float speed;

    public Rigidbody2D rb;


    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        MovementController();
    }
    public void MovementController()
    {
        float horizontal = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(horizontal, 0f)*speed;
    }
}
