using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPersonaje : MonoBehaviour
{

    public GameObject objectBullet;
    // Update is called once per frame
    void Update()
    {
        Disparo();
    }
    void Disparo()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject obj = Instantiate(objectBullet);
            obj.transform.position = transform.position;
        }
    }
}
