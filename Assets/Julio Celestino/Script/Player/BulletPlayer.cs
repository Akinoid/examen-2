using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPlayer : MonoBehaviour
{
    public float speed;
    public Vector2 direction;
    public Rigidbody2D rb;

    public float time;
    public float maxTime;

    // Start is called before the first frame update
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = direction * speed;
        Duracion();
    }
    public void Duracion()
    {
        time += Time.deltaTime;
        if (time >= maxTime)
        {
            Destroy(gameObject);
        }
    }
}
